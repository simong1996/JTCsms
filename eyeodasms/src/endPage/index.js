import React from 'react';

function EndPage(props) {
  return (
    <div className="endpage">
      <h1>{props.endpageMsg.title}</h1>
      <h2>{props.endpageMsg.para}</h2>
      <div className="linkform">
        <button type="button" onClick={() => props.changePage('sendLink') }>Go Back</button>
      </div>
    </div>
  );
}

export default EndPage;
