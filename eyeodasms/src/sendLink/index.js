import React from 'react';

function SendLink(props) {
  return (
    <div className="sendlink">
      <h1>Welcome</h1>
      <h2>Sending link with SMS</h2>
      <div className="linkform">
        <div className="input-container">
          <label htmlFor="name">Name</label>
          <input type="text" id="name" placeholder="John Doe" onChange={props.onChange}/>
        </div>
        <div className="input-container">
          <label htmlFor="number">Phone Number</label>
          <input type="text" id="number" placeholder="+46701234567" onChange={props.onChange} />
        </div>
        <div className="input-container">
          <label htmlFor="Link">Link for Call</label>
          <input type="text" id="link" onChange={props.onChange} />
        </div>
        <button type="button"onClick={props.sendSms}>Send Link</button>
      </div>
    </div>
  );
}

export default SendLink;
