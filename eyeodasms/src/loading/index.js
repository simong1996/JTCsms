import React from 'react';
import { BeatLoader } from 'react-spinners';

function Loading() {
  return (
    <div className="loading">
      <BeatLoader color={'#f89820'} />
    </div>
  );
}

export default Loading;
