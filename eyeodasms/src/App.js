import React from 'react';
import './App.css';
import SendLink from './sendLink';
import EndPage from './endPage';
import Loading from './loading';
const URL = 'https://donut.kjeld.io';

const PAGE = (send, change, cpage, appS) => ({
  sendLink: <SendLink sendSms={send} onChange={change}/>,
  loading: <Loading />,
  endPage: <EndPage changePage={cpage} endpageMsg={appS.endpageMsg}/>
});

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPage: 'sendLink',
      name: '',
      number: '',
      link: '',
      endpageMsg: ''
    };

    this.onChange = this.onChange.bind(this);
    this.sendSms = this.sendSms.bind(this);
    this.changePage = this.changePage.bind(this);
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          {PAGE(this.sendSms, this.onChange, this.changePage, this.state)[this.state.currentPage]}
        </div>
      </div>
    );
  }

  sendSms() {
    this.changePage('loading');
    let payload = {
      phoneNr: this.state.number,
      url: this.state.link,
      name: this.state.name
    };

    this.state.number.startsWith('+46') && this.state.number.length === 12 ? (
      fetch(`${URL}/jtcserver/customMsg`, {
        headers: {
          'Accept': 'aaplication/json',
          'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify(payload)
      })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        data.message.statusCode === 200 ? (
          this.changePage('endPage', {title: 'Success', para: 'Your sms was sent.'})
        ) : (
          this.changePage('endPage', {title: 'Error', para: 'Make sure the number format is: +46701234567'})
        );
      })
    ) : (
      this.changePage('endPage', {title: 'Error', para: 'Make sure the number format is: +46701234567'})
    )
  }

  onChange(event) {
    if(event.target.id === 'name') {
      this.setState({name: event.target.value});
  } else if (event.target.id === 'number') {
      this.setState({number: event.target.value});
  } else if (event.target.id === 'link') {
      this.setState({link: event.target.value});
    }
  }

  changePage(page, msg) {
    this.setState({currentPage: page, endpageMsg: msg});
  }
}

export default App;
