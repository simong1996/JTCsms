const https = require('https');
const querystring = require('querystring');
const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const port = 3000;
const app = express();
const username = 'u40213f9ab85999cb5d193da66b8d06fc';
const password = '4E6D55ECCA9E2D1116265D0D3204A4C9';
const key = new Buffer.from(username + ':' + password).toString('base64');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.listen(process.env.PORT || port, process.env.IP);

const sendSms = (req, res, next) => {
  let message =
  `Hej ${req.body.name}.\n\nOm någon minut är det dags för värderingen. Klicka på länken ${req.body.url} för att start samtalet.\n\nMvh Haninge Bilpark.`
  const postFields = {
    from: "+46766861836",
    to: req.body.phoneNr,
    message: message
  };
  const postData = querystring.stringify(postFields);
  const options = {
    hostname: 'api.46elks.com',
    path: '/a1/SMS',
    method: 'POST',
    headers: {
      'Authorization': 'Basic ' + key
    }
  };

  const callback = res => {
    var str = '';
    res.on('data', chunk => {
      str += chunk;
    });
    res.on('end', () => {
      if(res.statusCode === 200) {
        req.newStr = JSON.parse(str);
        req.newStr.statusCode = res.statusCode;
      } else {
        req.newStr = {
          statusCode: res.statusCode,
          error: str
        };
      }
      next();
    });
  };

  var request = https.request(options, callback);
  request.write(postData);
  request.end();
};

app.post('/jtcserver/customMsg', sendSms, (req, res) => {
    res.json({message: req.newStr});
  }
);


console.log(`App listening on port ${port}`);
